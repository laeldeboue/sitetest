$(function () {


    let downs = $('.menu-item')
    $(downs).css({
        color: 'wheat',
    })
    // let dropdowns = $('.d-menu')

    $.each($(downs), function () {
        $(this).hover(
            function (e) {
                $(this).children('.d-menu').slideDown()
                $(this).children('.d-menu').css({
                    position: 'absolute',
                    top: '52px'
                })
                $(this).css({
                    'background-color': "none",
                    color: "white"
                })
            },

            function () {
                if ($(this).children('.d-menu:hover') !== 0){
                    $(this).children('.d-menu').slideUp()
                    $(this).css({
                        color: "wheat"
                    })
                }
            }
        )
    })

    let cards = $('.card')
    $.each($(cards), function () {
        $(this).hover(
            function () {
                $(this).addClass(' selected')
            },

            function () {
                $(this).removeClass('selected')
            }
        )
    } )

    let shadows = $('.shadow')
    $.each($(shadows), function () {
        let z_index = $(this).css('z-index')
        $(this).hover(
            function () {
                $(this).addClass(' drop-shadow selected')
                .css({
                    'z-index': 1000,
                })
            },

            function () {
                $(this).removeClass('drop-shadow selected')
                .css({
                    'z-index': z_index,
                })
            }
        )
    })

    // let main_img = $('#main-image')
    // let main_img_src = main_img.attr('src')
    // let img_thumbnails = $('.img-miniature')
    // $.each($(img_thumbnails), function () {
    //     let thumbnail_src = $(this).attr('src')
    //     $(this).hover(
    //         function () {   
    //             $(main_img).attr('src', thumbnail_src)
    //         }, 
    //     )
    // })

    // $(main_img).mouseover(function () {
    //     $(this).attr('src', main_img_src)
    // })

    // Show dialog image
    // let img_miniatures = $('.img-miniature')
    // $.each($(img_miniatures), function() {
    //     $(this).click(
    //         () => {
    //             let tool_id = $(this).attr('id')
    //             let dialogs = $('.dialog')
    //             let selector
    //             let dialog = $.each($(dialogs), function () {
    //                 let dialog_id = tool_id
    //                 selector = $(this).id(tool_id);
    //             })

    //             // Metro.dialog.open('#'+ $(selector).id())
    //         }
    //     )
    // })
})
