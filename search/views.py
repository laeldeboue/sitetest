from django.shortcuts import render, redirect, reverse
from siteweb.models import ToolCategory, ToolType, SpecificTool, ToolGroup
from django.db.models import Q


def searchView(request):
    try:
        q = request.GET.get('query')
    except:
        q = None
    if q:
        tooltypes = ToolType.objects.all()
        categories = ToolCategory.objects.all()
        queries = SpecificTool.objects.filter(
            # Q(subproduct__label_sub_product__icontains=q) |
            Q(specifictool_ref__icontains=q) |
            Q(specifictool_name__icontains=q) |
            Q(specifictool_description__icontains=q)
        ).distinct()
        template = "search/query_results.html"
        context = {
            "query": q, 
            "queries": queries,
            "tooltypes": tooltypes,
            "categories": categories
            }
        return render(request, template, context)
    else:
        return redirect(reverse('main:home'))
