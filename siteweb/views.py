
from django.shortcuts import render, get_object_or_404, render_to_response
from django.contrib.sites.shortcuts import get_current_site
from django.template import RequestContext
from django.views.generic import ListView, DetailView
from .models import (
    ToolCategory,
    ToolType,
    ImageSlider,
    Marque,
    ToolGroup,
    SpecificTool,
    About,
    EmailProImage,
)


class CategoryToolListView(ListView):
    model = ToolCategory
    template_name = "siteweb/index.html"
    context_object_name = "categories"

    def get_queryset(self):
        return ToolCategory.objects.all().order_by('id')

    def get_context_data(self, *, object_list=None, **kwargs):
        try:
            about = About.objects.last()
        except About.DoesNotExist:
            about = About.objects.none()

        context = super(CategoryToolListView, self).get_context_data(**kwargs)
        context['slider'] = ImageSlider.objects.all()
        context['about'] = about
        return context


class ToolCategoryDetailView(DetailView):
    model = ToolCategory
    template_name = "siteweb/tool_category_detail.html"
    context_object_name = 'category'

    def get_context_data(self, **kwargs):
        context = super(ToolCategoryDetailView, self).get_context_data(**kwargs)
        return context


class ToolTypeDetailView(DetailView):
    model = ToolType
    template_name = "siteweb/tool_type_detail.html"

    def get_context_data(self, **kwargs):
        context = super(ToolTypeDetailView, self).get_context_data(**kwargs)
        category = self.object.category
        context['tool_category'] = ToolCategory.objects.get(id=category.id)
        context['tool_type'] = get_object_or_404(ToolType,
                                                 id=kwargs['object'].id,
                                                 tooltype_slug=str(kwargs['object'].tooltype_slug)
                                                 )
        return context


class ToolGroupDetailView(DetailView):
    model = ToolGroup
    template_name = "siteweb/tool_group_detail.html"
    context_object_name = "tool_group"

    def get_context_data(self, **kwargs):
        context = super(ToolGroupDetailView, self).get_context_data(**kwargs)
        # get_categories(context)
        return context


class SpecificToolDetailView(DetailView):
    model = SpecificTool
    template_name = "siteweb/specific_tool.html"
    context_object_name = 'specific_tool'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # tooltype_id = kwargs['object'].toolgroup.id
        tool_id = kwargs['object'].pk
        context["tool"] = get_object_or_404(SpecificTool,
                                            pk=tool_id,
                                            specifictool_slug=kwargs["object"].specifictool_slug
                                            )
        return context


def brand_list(request):
    brands = Marque.objects.all().distinct("name").order_by('name')
    context = {
        'brands': brands,
    }
    return render(request, 'siteweb/brands.html', context)


def catalog_list(request):
    template_name = 'siteweb/catalogs.html'
    return render(request, template_name)


class ImageEmailProList(ListView):
    model = EmailProImage
    template_name = 'siteweb/emailpro.html'
    context_object_name = 'email_images'


class ImageEmailProDetail(DetailView):
    model = EmailProImage
    template_name = 'siteweb/image-detail.html'
    context_object_name = 'image'

    def get_context_data(self, **kwargs):
        context = super(ImageEmailProDetail, self).get_context_data(**kwargs)
        context['full_url'] = ''.join(['https://', get_current_site(self.request).domain])
        return context


def error_404(request, exception):
    return render(request, '404.html')

