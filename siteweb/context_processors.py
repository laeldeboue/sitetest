from .models import ToolCategory


def category_list(request):
    categories = ToolCategory.objects.all().order_by('id')
    return {'categories': categories}

