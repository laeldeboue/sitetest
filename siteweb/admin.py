from django.contrib import admin
from .models import (
    ToolCategory,
    ToolType,
    ImageSlider,
    Marque,
    SpecificTool,
    ImagesTool,
    ToolGroup,
    About,
    EmailProImage,
)

from django_summernote.admin import SummernoteModelAdmin
# from markitup.widgets import AdminMarkItUpWidget


class ImageSliderInline(admin.TabularInline):
    model = ImageSlider
    extra = 0


class ImagesToolInline(admin.TabularInline):
    model = ImagesTool
    extra = 2


class ToolGroupInline(admin.TabularInline):
    model = SpecificTool
    extra = 0


class ToolTypeInline(admin.TabularInline):
    model = ToolType
    inlines = [ToolGroupInline]
    extra = 0


@admin.register(ToolCategory)
class ToolCategoryAdmin(admin.ModelAdmin):
    list_filter = ['category_name']
    list_display = ["category_name", 'category_image', 'catalog']
    exclude = ('category_slug',)

    inlines = [ToolTypeInline, ImageSliderInline]


@admin.register(ToolType)
class ToolTypeAdmin(admin.ModelAdmin):
    list_display = ["tooltype_description", "category", ]
    list_filter = ["category",]
    # autocomplete_fields = ['fabricant']
    exlude = ('tooltype_slug',)
    fieldsets = (
        ("Catégorie", {
            "fields": ("category", "tooltype_description", 'tooltype_image'),
        }),
    )


@admin.register(ToolGroup)
class ToolGroupAdmin(admin.ModelAdmin):
    list_filter = ['tooltype']
    list_display = ['toolgroup_name', 'tooltype']
    exclude = ('toolgroup_slug',)
    fieldsets = (
        ("Groupe d'outils", {
            "fields": (
                'tooltype',
                'toolgroup_name',
                'toolgroup_image',
            ),
        }),
    )


@admin.register(SpecificTool)
class SpecificToolAdmin(SummernoteModelAdmin):
    # summernote_fields = "__all__"
    summernote_fields = ('specifictool_description',)
    list_display = [
        "specifictool_name",
        "specifictool_ref",
        "toolgroup",
        "displayMarque",
        "tool_video_url",
    ]
    list_per_page = 10
    search_fields = ['specifictool_name', 'specifictool_ref']
    list_filter = ["toolgroup",]
    autocomplete_fields = ['marque']
    inlines = [ImagesToolInline]
    # fieldsets = (
    #     ("Outils", {
    #         "fields": (
    #             "toolgroup",
    #             "specifictool_name",
    #             'specifictool_ref',
    #             'specifictool_image', 
    #             'tool_video_url',
    #             "fiche_technique",
    #         ),
    #     }),

    #     ("Marque", {
    #         "fields": ("marque",)
    #     })
    # )


@admin.register(About)
class AboutAdmin(SummernoteModelAdmin):
    list_display = ['presentation', 'short_content']
    summernote_fields = ('presentation',)

    def short_content(self, obj):
        return obj.presentation[:40] + "..."

    short_content.short_description = 'Résumé'


@admin.register(ImagesTool)
class ImagesToolAdmin(admin.ModelAdmin):
    list_display = ['tool', 'image_tool']
    list_filter = ['tool',]


@admin.register(Marque)
class MarqueAdmin(admin.ModelAdmin):
    list_display = ['name', 'logo_marque',]
    empty_value_display = "Inconnue"
    search_fields = ('marque',)


@admin.register(ImageSlider)
class ImageSliderAdmin(admin.ModelAdmin):
    list_display = ["category_tool", "title",]
    list_filter = ["category_tool",]
    empty_value_display = "Inconnue"


@admin.register(EmailProImage)
class EmailProImageAdmin(admin.ModelAdmin):
    list_display = ['description', 'image_file']


# admin.site.register(SpecificTool, SpecificToolAdmin)
admin.AdminSite.site_header = "Anastasy West Africa & Distribution"
admin.AdminSite.site_title = "AWAID ADMINISTRATION"
