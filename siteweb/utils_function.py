import os, uuid


def get_filename_extension(filepath):
    basename = os.path.basename(filepath)
    filename, file_extension = os.path.splitext(basename)

    return filename, file_extension


def slider_filename(instance, filename):
    rand = str(uuid.uuid4())[:11:-1]
    name, ext = get_filename_extension(filename)

    final_filename = "slider/{0}{1}".format(rand, ext)
    return final_filename


def tooltype_filename(instance, filename):
    rand = str(uuid.uuid4())[:11:-1]
    name, ext = get_filename_extension(filename)

    final_filename = "tooltypes/{0}{1}".format(rand, ext)
    return final_filename


def toolgroup_filename(instance, filename):
    rand = str(uuid.uuid4())[:11:-1]
    name, ext = get_filename_extension(filename)

    final_filename = "toolgroups/{0}{1}".format(rand, ext)
    return final_filename


def tool_filename(instance, filename):
    rand = str(uuid.uuid4())[:11:-1]
    name, ext = get_filename_extension(filename)

    final_filename = "tools/{0}{1}".format(rand, ext)
    return final_filename


def fiche_tech(instance, filename):
    rand = str(uuid.uuid4())[:11:-1]
    name, ext = get_filename_extension(filename)

    final_filename = "fiches/{0}{1}".format(rand, ext)
    return final_filename


def logo_filename(instance, filename):
    rand = str(uuid.uuid4())[:11:-1]
    name, ext = get_filename_extension(filename)

    final_filename = "logos/{0}{1}".format(rand, ext)
    return final_filename


def category_filename(instance, filename):
    rand = str(uuid.uuid4())[:11:-1]
    name, ext = get_filename_extension(filename)

    final_filename = "categories/{0}{1}".format(rand, ext)
    return final_filename


def get_youtube_embed_url(url_link):
    if url_link:
        embed_url = url_link.replace('watch?v=', 'embed/')
        return embed_url
    return url_link


def get_email_image_name(instance, filename):
    rand = str(uuid.uuid4())
    name, ext = get_filename_extension(filename)

    final_filename = "EmailPro/{0}{1}".format(rand, ext)
    return final_filename


def get_catalog_filename(instance, filename):
    name, ext = get_filename_extension(filename)
    final_filename = "Catalogs/{0}{1}".format(instance.category_slug, ext)
    return final_filename
