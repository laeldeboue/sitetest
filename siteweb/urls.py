from django.urls import path
from .views import (
    CategoryToolListView, 
    ToolCategoryDetailView, 
    ToolTypeDetailView, 
    SpecificToolDetailView,
    ToolGroupDetailView,
    brand_list,
    ImageEmailProList,
    ImageEmailProDetail,
    catalog_list
    )


app_name = "main"
urlpatterns = [
    path('', CategoryToolListView.as_view(), name='home'),
    path('categories/<int:pk>/<slug:slug>', 
        ToolCategoryDetailView.as_view(), 
        name="category-detail"
    ),

    path('tooltypes/<int:pk>/<slug:slug>', 
        ToolTypeDetailView.as_view(), 
        name="tooltype"
    ),

    path('toolgroups/<int:pk>/<slug:slug>', 
        ToolGroupDetailView.as_view(), 
        name="toolgroup"
    ),

    path('tools/<int:pk>-<slug:slug>',
        SpecificToolDetailView.as_view(), 
        name="specifictool"
    ),

    path('brands/', brand_list, name="brands"),
    path('emailpro/', ImageEmailProList.as_view(), name="imagelist"),
    path('emailpro/image/<int:pk>/', ImageEmailProDetail.as_view(), name="imagepro"),

    path('catalogs/', catalog_list, name="catalogs"),

]