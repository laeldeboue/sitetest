from fileinput import filename

from django.db import models
from django.urls import reverse
from django.utils.text import slugify, gettext_lazy as _
from django.utils.html import format_html

from .utils_function import (
    category_filename,
    tool_filename,
    logo_filename,
    slider_filename,
    fiche_tech,
    tooltype_filename,
    toolgroup_filename,
    get_youtube_embed_url,
    get_email_image_name, get_catalog_filename,
)


class ToolCategory(models.Model):
    category_name = models.CharField(
        verbose_name="Libellé catégorie",
        max_length=100,
        unique=True,
    )

    category_slug = models.SlugField(
        unique=True,
    )

    category_image = models.ImageField(
        upload_to=category_filename,
        verbose_name="Image Catégorie",
        max_length=250,
        default='categories/default.jpg',
    )

    position = models.IntegerField(
        help_text="Préciser la position de cette catégorie dans menu",
        null=True, blank=True
    )
    catalog = models.FileField("Catalogue", upload_to=get_catalog_filename)

    class Meta:
        ordering = ["position", 'category_name', ]
        verbose_name = "Catégorie d'outils"
        verbose_name_plural = "Catégories"

    def __str__(self):
        return self.category_name

    def save(self, **kwargs):
        self.category_slug = slugify(self.category_name)
        super(ToolCategory, self).save()

    def get_absolute_url(self):
        return reverse('main:category-detail',  # product-detail
                       kwargs={
                           "pk": str(self.id),
                           "slug": self.category_slug,
                       }
                   )


class ToolType(models.Model):
    tooltype_description = models.CharField(
        verbose_name="Groupe d'outils",
        unique=True,
        max_length=100,
        null=False
    )
    tooltype_image = models.ImageField(
        _("Image"),
        upload_to=tooltype_filename,
        height_field=None,
        width_field=None,
        max_length=250,
        default="tooltypes/default.jpg"
    )
    category = models.ForeignKey(
        ToolCategory,
        on_delete=models.CASCADE,
        related_name="toolcategories",
        related_query_name="toolcategory",
        verbose_name="Catégorie"
    )

    tooltype_slug = models.SlugField(unique=True)

    class Meta:
        ordering = ['tooltype_description', ]
        verbose_name = "Type d'outil"
        verbose_name_plural = "Type d'outils"

    def __str__(self):
        return "%s" % self.tooltype_description

    def save(self, **kwargs):
        self.tooltype_slug = slugify(self.tooltype_description)
        super(ToolType, self).save()

    def get_absolute_url(self):
        return reverse('main:tooltype',  # sub-product-detail
                       kwargs={
                           "slug": self.tooltype_slug,
                           'pk': str(self.id)
                       })


class ToolGroup(models.Model):
    toolgroup_name = models.CharField(
        _("Nom du groupe d'outils"),
        max_length=200,
        unique=True
    )
    toolgroup_image = models.ImageField(
        _("Image du groupe d'outils"),
        upload_to=toolgroup_filename,
        height_field=None,
        width_field=None,
        max_length=250
    )
    tooltype = models.ForeignKey(
        ToolType,
        verbose_name=_("Type d'outils"),
        on_delete=models.CASCADE,
        related_name="tooltypes"
    )
    toolgroup_slug = models.SlugField(unique=True)

    class Meta:
        verbose_name = "Groupe d'outils"
        ordering = ['toolgroup_name']

    def __str__(self):
        return self.toolgroup_name

    def save(self, **kwargs):
        self.toolgroup_slug = slugify(self.toolgroup_name)
        return super(ToolGroup, self).save()

    def get_absolute_url(self):
        return reverse("main:toolgroup", kwargs={
            "pk": str(self.id),
            "slug": self.toolgroup_slug,
        }
                       )


class SpecificTool(models.Model):
    specifictool_name = models.CharField(
        verbose_name="Nom de l'outil",
        max_length=100,
    )
    specifictool_ref = models.CharField(
        verbose_name="Code de référence",
        unique=True,
        max_length=10,
    )
    specifictool_image = models.ImageField(
        _("Image de l'outil"),
        upload_to=tool_filename,
        height_field=None,
        width_field=None,
        max_length=250,
        default="tools/default.jpg"
    )
    tool_video_url = models.CharField(_("Vidéo URL"), null=True, blank=True, unique=False,
                                      max_length=150, help_text="Inserer l'URL Youtube de la vidéo", )
    specifictool_description = models.TextField(
        help_text="Description de l'outil",
        null=True,
    )
    toolgroup = models.ForeignKey(
        ToolGroup,
        verbose_name="Groupe d'outils",
        on_delete=models.CASCADE,
        related_name="toolgroups",
        related_query_name="toolgroup"
    )
    fiche_technique = models.FileField(
        upload_to=fiche_tech,
        max_length=250,
        null=True,
        blank=True,
    )
    specifictool_slug = models.SlugField()
    marque = models.ManyToManyField(
        "Marque",
    )

    def __str__(self):
        return self.specifictool_name

    def get_absolute_url(self):
        return reverse("main:specifictool",  # single-tool
                       kwargs={
                           "pk": self.id,
                           "slug": self.specifictool_slug,
                       }
                       )

    class Meta:
        verbose_name = "Outil spécific"
        unique_together = ('specifictool_name', 'specifictool_ref')

    def save(self, **kwargs):
        self.specifictool_slug = slugify(self.specifictool_name)
        self.tool_video_url = get_youtube_embed_url(self.tool_video_url)
        super(SpecificTool, self).save()

    def displayMarque(self):
        return ', '.join(marque.name for marque in self.marque.all())

    def displayDescription(self):
        if len(format_html(self.specifictool_description)) < 300:
            return format_html(self.specifictool_description)
        else:
            return format_html(self.specifictool_description[:300] + '...')

    displayDescription.short_description = "Description"
    displayMarque.short_description = "Marque de l'outil"


class Marque(models.Model):
    name = models.CharField(
        verbose_name="Marque",
        max_length=100,
        null=True
    )

    logo_marque = models.ImageField(
        upload_to=logo_filename,
        help_text="Logo de la marque",
    )

    class Meta:
        ordering = ["name", ]
        verbose_name = "Marque"

    def __str__(self):
        return self.name


class ImagesTool(models.Model):
    image_tool = models.ImageField(
        _("Image de l'outil"),
        upload_to=tool_filename,
        height_field=None,
        width_field=None,
        max_length=250
    )
    tool = models.ForeignKey(
        SpecificTool,
        on_delete=models.CASCADE,
        related_name="tools",
        verbose_name=_("Outil"),
        null=False,
        blank=False,
    )

    class Meta:
        verbose_name = "Image de l'outil"

    def __str__(self):
        return self.tool.specifictool_name


class ImageSlider(models.Model):
    category_tool = models.ForeignKey(
        ToolCategory,
        on_delete=models.CASCADE,
        related_name="imagesliders",
        related_query_name="image"
    )

    title = models.CharField(
        verbose_name="Titre de l'image",
        max_length=100,
        null=True,
        blank=True
    )

    image_slide = models.ImageField(
        upload_to=slider_filename,
        verbose_name="Image du produit",
        help_text="Image de défilement de la page d'accueil"
    )

    class Meta:
        verbose_name = "Image Défilante"

    def __str__(self):
        return '%s' % self.category_tool.category_name


class About(models.Model):
    presentation = models.TextField(help_text="Saisir ici la présentaion de l'entreprise")

    def __str__(self):
        return self.presentation

    class Meta:
        verbose_name = "Présentation"


class EmailProImage(models.Model):
    description = models.CharField(max_length=200, null=True, blank=True, default="Image")
    image_file = models.ImageField("Image", upload_to=get_email_image_name)

    def __str__(self):
        return self.description

    def get_absolute_url(self):
        return reverse('main:imagepro', kwargs={"pk": self.pk})
